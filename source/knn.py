# -*- coding: utf-8 -*-
# install pygame, python-dateutil, matplotlib, pillow, numpy, sklearn,
#pyparsing, scikit-learn, scipy
from __future__ import division

from common.logger import log
import common.imaging as imaging
import common.dataset as datasets
import common.metrics as metrics

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from PIL import Image

from sklearn.model_selection import cross_val_score
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import learning_curve
from sklearn.neighbors import KNeighborsClassifier
from sklearn.externals import joblib
from sklearn.metrics import classification_report

import numpy as np
import os, shutil
import sys
import random

# the dataset we are gonna use here to test real images
DATASET = 'sfa'

GT_DATABASE_PATH = '../datasets/img_databases/'+ DATASET +'/GT/'
PREDICTED_DATABASE_PATH = '../datasets/img_databases/'+ DATASET +\
                        '/PREDICTED/knn/'
ORI_DATABASE_PATH = '../datasets/img_databases/'+ DATASET +'/ORI/'


# used to keep the trainned model
KNN_DUMP_MODEL_PATH = './knn/knn_model.pkl'

# Number of folds used in the trainning
KNN_FOLDS=10

# Image database used in the experiments. You may set up a skin and a non
# skin dataset based on a window size.
# Window size availables: [1,3,5,7,9,11,13,15,17,19,21,23,25,27,29,31,33,35]
WIN_SIZE = 9
SKIN_IMG_DATABASE = '../datasets/img_databases/sfa/SKIN/'+ str(WIN_SIZE) +'/'
NON_SKIN_IMG_DATABASE = '../datasets/img_databases/sfa/NS/'+ str(WIN_SIZE) +'/'

# dataset classes
SKIN = 1
NON_SKIN = -1


def report(classifier, X_test, Y_test):
    Y_pred = classifier.predict(X_test)
    # ns - non skin :: yS - skin
    target_names = ['nS', 'yS']
    log(classification_report(Y_test, Y_pred, target_names=target_names))


def train_knn_with_cross_validation(dataset):
    """
    We are using COLOR_RGB as the default database. Pay attention with other
    methods when changing this parameter for trainning here.
    """
    X_train, Y_train, X_test, Y_test = datasets.load_data(dataset)

    log('Trainning the data with '+ str(KNN_FOLDS) + ' folds')
    log('X_train length '+ str(len(X_train)))
    log('X_test length ' + str(len(X_test)))

    knn_classifier = KNeighborsClassifier(n_neighbors=15, weights='uniform')
    scores = cross_val_score(knn_classifier, X_train, Y_train, cv=KNN_FOLDS, \
                             verbose=2)
    log('Scores: ' + str(scores))
    log('Accuracy: %0.2f (+/- %0.2f)' % (scores.mean(), scores.std()))

    knn_classifier.fit(X_train, Y_train)
    log('Cross validation trainning finished')
    report(knn_classifier, X_test, Y_test)

    return knn_classifier


def train_knn_with_cross_validation_and_grid_search(dataset):
    X_train, Y_train, X_test, Y_test = datasets.load_data(dataset)

    log('Trainning the data with '+ str(KNN_FOLDS) + \
        ' folds and optimized parameters')
    log('X_train length '+ str(len(X_train)))
    log('X_test length ' + str(len(X_test)))

    param_grid = [{
                    'n_neighbors': [3, 5, 9, 15, 25, 31, 35],
                    'weights': ['distance', 'uniform'],
                    'algorithm': ['auto']
                 }]

    knn_classifier = GridSearchCV(KNeighborsClassifier(), param_grid, \
                                  cv=KNN_FOLDS, verbose=2)
    knn_classifier.fit(X_train, Y_train)

    log(80*'-')
    log('Best estimator found by grid search:')
    log('Accuracy: %0.2f (+/- %0.2f)' % (knn_classifier.best_score_.mean(),
                                        knn_classifier.best_score_.std()))
    log('Best score: ' + str(knn_classifier.best_score_))
    log('Best estimator: ' + str(knn_classifier.best_estimator_))
    log('Best weights: ' + str(knn_classifier.best_estimator_.weights))
    log('Best number of neighbors: ' + \
        str(knn_classifier.best_estimator_.n_neighbors))
    log('Cross validation with grid search trainning finished')
    report(knn_classifier, X_test, Y_test)

    return knn_classifier


def test_images_dataset():
    """
    Get images from the ORI_DATABASE_PATH to test based on the
    trainned model.
    """
    datasets.mk_predicted_dir(PREDICTED_DATABASE_PATH)

    files = os.listdir(ORI_DATABASE_PATH)
    for img in files:
        # original image
        img_ori_path = ORI_DATABASE_PATH + img
        pred_img = datasets.replace_file_extension(DATASET, 'ori', 'pre', img)

        if os.path.exists(PREDICTED_DATABASE_PATH + pred_img):
            continue

        log(img_ori_path)

        # test the original image and concat the GT
        predicted_img = test_image(img_ori_path)
        predicted_img.save(PREDICTED_DATABASE_PATH + pred_img)


def test_image(img_path):
    knn_classifier = joblib.load(KNN_DUMP_MODEL_PATH)

    rgb_img = imaging.open_rgb_image(img_path)

    width, height = rgb_img.size
    result = np.zeros_like(rgb_img)

    for x in range(0, width):
        for y in range(0, height):
            r, g, b = rgb_img.getpixel((x, y))

            # both UCI and SFA datasets are in BGR order of channels
            # this reshape is due scikit-learn warning
            sample = np.array([b, g, r]).reshape(1, -1)
            decision = knn_classifier.predict(sample)

            if decision == SKIN:
                result[y, x] = [255, 255, 255]
            else:
                result[y, x] = [0, 0, 0]

    # creates a new image based on predicted result array
    return Image.fromarray(result, 'RGB')


def mk_knn_output_dir():
    directory = os.path.dirname(KNN_DUMP_MODEL_PATH)
    if not os.path.exists(directory):
        os.makedirs(directory)


options = sys.argv[1:]
option = ''

if len(options):
    option = options[0]

if option == '1':
    datasets.generate_sfa_dataset(SKIN_IMG_DATABASE, SKIN, \
                                  NON_SKIN_IMG_DATABASE, NON_SKIN)

elif option == '2':
    knn_classifier = train_knn_with_cross_validation('uci')
    mk_knn_output_dir()
    joblib.dump(knn_classifier, KNN_DUMP_MODEL_PATH)

elif option == '3':
    knn_classifier = train_knn_with_cross_validation_and_grid_search('uci')
    mk_knn_output_dir()
    joblib.dump(knn_classifier, KNN_DUMP_MODEL_PATH)

elif option == '4':
    test_images_dataset()

elif option == '5':
    datasets.count_samples_repeatability('uci')

else:
    log('Invalid option ['+ option +']')
