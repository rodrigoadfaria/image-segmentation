"""
===============================================
Local Binary Pattern for texture classification
===============================================

In this example, we will see how to classify textures based on LBP (Local
Binary Pattern). The histogram of the LBP result is a good measure to classify
textures. For simplicity the histogram distributions are then tested against
each other using the Kullback-Leibler-Divergence.
"""

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import scipy.ndimage as nd
import skimage.feature as ft
from skimage import data
from PIL import Image
import os

# for the histogram computation
from scipy.spatial import distance as dist
import matplotlib.pyplot as plt
import numpy as np
import argparse
import glob
import cv2

# settings for LBP
METHOD = 'uniform'
P = 16
R = 2
matplotlib.rcParams['font.size'] = 9


def compute_histograms(dataset):
    index = {}
    images = {}

    # loop over the image paths
    for image_path in glob.glob(dataset + "/*.jpg"):
        # extract the image filename (assumed to be unique) and
        # load the image, updating the images dictionary
        filename = image_path[image_path.rfind("/") + 1:]
        image = cv2.imread(image_path)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        index[filename] = compute_hist(image)
    return index


def compute_hist(image):
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

    # extract a 3D RGB color histogram from the image,
    # using 8 bins per channel, normalize, and update
    # the index
    hist = cv2.calcHist([image], [0, 1, 2], None, [8, 8, 8],
        [0, 256, 0, 256, 0, 256])
    hist = cv2.normalize(hist, hist).flatten()

    return hist


# For Correlation and Intersection, LARGER value indicates higher similarity.
# For Chi-Squared and Hellinger, a SMALLER value indicates higher similarity.
def compare_histograms(index, hist_to_check):
    # METHOD #1: UTILIZING OPENCV
    # initialize OpenCV methods for histogram comparison
    OPENCV_METHODS = (
        ("Correlation", cv2.HISTCMP_CORREL),
        ("Intersection", cv2.HISTCMP_INTERSECT),
        ("Chi-Squared", cv2.HISTCMP_CHISQR),
        ("Hellinger", cv2.HISTCMP_BHATTACHARYYA))

    # loop over the comparison methods
    methodName, method = OPENCV_METHODS[2]
    # initialize the results dictionary and the sort
    # direction
    results = {}
    reverse = False

    # if we are using the correlation or intersection
    # method, then sort the results in reverse order
    if methodName in ("Correlation", "Intersection"):
        reverse = True

    # loop over the index
    for (k, hist) in index.items():
        # compute the distance between the two histograms
        # using the method and update the results dictionary
        d = cv2.compareHist(hist_to_check, hist, method)
        results[k] = d

    # sort the results
    results = sorted([(v, k) for (k, v) in results.items()], reverse = reverse)
    return methodName, results


def kullback_leibler_divergence(p, q):
    p = np.asarray(p)
    q = np.asarray(q)
    filt = np.logical_and(p != 0, q != 0)
    return np.sum(p[filt] * np.log2(p[filt] / q[filt]))


def match(refs, img):
    best_score = 10
    best_name = None
    lbp = ft.local_binary_pattern(img, P, R, METHOD)
    hist, _ = np.histogram(lbp, normed=True, bins=P + 2, range=(0, P + 2))
    for name, ref in refs.items():
        ref_hist, _ = np.histogram(ref, normed=True, bins=P + 2,
                                   range=(0, P + 2))
        score = kullback_leibler_divergence(hist, ref_hist)
        if score < best_score:
            best_score = score
            best_name = name

    return best_name, best_score


def test_image_texture(img_path, refs, lbp=False, threshold_score=0.9):
    img_gs = cv2.imread(img_path, cv2.IMREAD_GRAYSCALE)
    img_color = cv2.imread(img_path)
    width, height, depth = img_color.shape

    # we are using 35x35 window to test patches in the image, but probably it
    # might be dynamically choosen based on the training size
    window = 35
    w_start = 0
    h_start = 0

    output = np.zeros((width, height, depth), np.uint8)

    while w_start < width:
        h_start = 0
        while h_start < height:
            crop = img_color[w_start:w_start+window, h_start:h_start+window]
            print crop.shape
            if lbp:
                crop_gs = img_gs.crop((w_start, h_start, w_start+window, h_start+window))
                b_name, b_score = match(refs, crop_gs)
            else:
                hist_ = compute_hist(crop)
                method_name, results = compare_histograms(refs, hist_)
                #print results
                b_score = results[0][0]

            if b_score >= threshold_score:
                print b_score
                #output[w_start:w_start+window, h_start:h_start+window] = crop
            h_start += window

        w_start += window

    cv2.imshow('image', img_color)
    cv2.imshow('output', output)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


def train_lbp_images(training_imgs_dir):
    images = os.listdir(TRAINING_TEX_IMAGES_DIR)

    refs = {}
    for file in images:
        train_img = Image.open(TRAINING_TEX_IMAGES_DIR + file).convert('L')
        ref = ft.local_binary_pattern(train_img, P, R, METHOD)
        refs[file] = ref

    return refs


TRAINING_TEX_IMAGES_DIR='../datasets/sfa_random_selected/'
img_path_to_test='../datasets/selected/124511719065943_2.jpg'

# computing by lbp
#refs = train_lbp_images(TRAINING_TEX_IMAGES_DIR)
#test_image_texture(img_path_to_test, refs)


# computing by histogram comparison
refs = compute_histograms(TRAINING_TEX_IMAGES_DIR)
test_image_texture(img_path_to_test, refs)


#image = cv2.imread(img_path_to_test)
#print compute_hist(image)
#np_img = np.asarray(Image.open(img_path_to_test))
#print image == np_img

#hist_ = compute_hist(image)
#method_name, results = compare_histograms(refs, hist_)
#print method_name
#for (k, v) in results:
#    print k, v


# plot histograms of LBP of textures
"""
fig, ((ax1, ax2, ax3), (ax4, ax5, ax6)) = plt.subplots(nrows=2, ncols=3,
                                                       figsize=(9, 6))
plt.gray()

ax1.imshow(brick)
ax1.axis('off')
ax4.hist(refs['brick'].ravel(), normed=True, bins=P + 2, range=(0, P + 2))
ax4.set_ylabel('Percentage')

ax2.imshow(grass)
ax2.axis('off')
ax5.hist(refs['grass'].ravel(), normed=True, bins=P + 2, range=(0, P + 2))
ax5.set_xlabel('Uniform LBP values')

ax3.imshow(wall)
ax3.axis('off')
ax6.hist(refs['wall'].ravel(), normed=True, bins=P + 2, range=(0, P + 2))

plt.show()"""
