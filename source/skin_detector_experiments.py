from __future__ import division
import matplotlib
matplotlib.use('Agg')
from matplotlib.backends.backend_pdf import PdfPages

import sys
import os
import platform
from time import sleep
from subprocess import Popen, PIPE
import threading
import re

import numpy as np
import csv
from PIL import Image
import matplotlib.pyplot as plt

import common.imaging as imaging
from common.logger import log
import common.dataset as datasets
import common.metrics as metrics

# Dataset
global DATASET
DATASET = 'pratheepan'

# Determine which variation of the skin detector binary is being used:
#   1. cbs - the original rule by Nadia Brancati
#   2. crs - the reversed rule based on Cr component
#   3. cmb - #1 and #2 rules combined
#   4. ngh - the combined rule with neighborhood approach
global SKIN_DETECTOR_VAR
SKIN_DETECTOR_VAR = 'cmb'

# Skin detector binary path, using from windows by default
global SKIN_DETECTOR_BIN
SKIN_DETECTOR_BIN = '../bin/windows/skin-detector-'+ SKIN_DETECTOR_VAR +'.exe'

global GT_DATABASE_PATH
global PREDICTED_DATABASE_PATH
global ORI_DATABASE_PATH
global PREDICTED_TRAPEZIA_PATH
GT_DATABASE_PATH = '../datasets/img_databases/'+ DATASET +'/GT/'
PREDICTED_DATABASE_PATH = '../datasets/img_databases/'+ DATASET +\
                        '/PREDICTED/'+ SKIN_DETECTOR_VAR + '/'
ORI_DATABASE_PATH = '../datasets/img_databases/'+ DATASET +'/ORI/'
# path for the trapezia generated from the method
PREDICTED_TRAPEZIA_PATH = PREDICTED_DATABASE_PATH + 'trapezia/'

global Y_MIN_PERCENTILE
global Y_MAX_PERCENTILE
Y_MIN_PERCENTILE=5
Y_MAX_PERCENTILE=95

# used for grid search results
global GS_DATA_PATH
global GS_PDF_PATH
global GS_CSV_PATH
GS_DATA_PATH = '../grid_search/'+ DATASET +'.npy'
GS_PDF_PATH = '../grid_search/'+ DATASET +'.pdf'
GS_CSV_PATH = '../grid_search/'+ DATASET +'.csv'

# These are used to know if a pixel is skin or not skin
# 765 is the sum of the 3 channels of the white pixel
# from the binarized predicted image (255, 255, 255)
SKIN = 765
# The grounth truth skin pixel is like the previous,
# but it may change for each dataset
# (e.g. sfa/pratheepan (255, 255, 255), while hgr (0, 0, 0)
GT_SKIN = 0 if DATASET == 'hgr' else 765


def test_image(img_predicted_path, img_ground_truth_path):
    rgb_img = imaging.open_rgb_image(img_predicted_path)
    rgb_gt_img = imaging.open_rgb_image(img_ground_truth_path)

    width, height = rgb_img.size
    log_message = "Image size "+ str(width) + "x" + str(height)
    log_message += " :: Number of samples "+ str(width*height)

    confusion_matrix = [[0, 0],
                        [0, 0]]

    '''
    confunsion matrix
          -------------------------------------
          |       Yes       |       No        |
    -------------------------------------------
    | Yes | True Positive   | False Negative  |
    -------------------------------------------
    | No  | False Positive  | True Negative   |
    -------------------------------------------
    '''
    for x in range(0, width):
        for y in range(0, height):
            r, g, b = rgb_img.getpixel((x, y))
            r1, g1, b1 = rgb_gt_img.getpixel((x, y))
            decision = r + g + b

            if decision == SKIN:
                if (r1+g1+b1 == GT_SKIN): # skin pixel from ground truth
                    confusion_matrix[0][0] += 1 # true positive
                else:
                    confusion_matrix[1][0] += 1 # false positive
            else:
                if (r1+g1+b1 == GT_SKIN): # skin pixel from ground truth
                    confusion_matrix[0][1] += 1 # false negative
                else:
                    confusion_matrix[1][1] += 1 # true negative

    rgb_img.close()
    rgb_gt_img.close()

    # gets the image name without the file extension
    im_original_name = os.path.basename(img_predicted_path)
    im_original_name = os.path.splitext(im_original_name)[0]

    false_positive = confusion_matrix[1][0]
    false_negative = confusion_matrix[0][1]
    true_positive  = confusion_matrix[0][0]
    true_negative  = confusion_matrix[1][1]

    return true_negative, true_positive, false_negative, false_positive


def compute_metrics(is_print_sample=True, is_print_total=True):
    images = os.listdir(GT_DATABASE_PATH)

    if is_print_sample:
        print '{0:60} {1:20} {2:20} {3:20} {4:20}'.format('Image', \
            'Precision', 'Recall', 'Specificity', 'F-measure')
    data = [['Image', 'Precision', 'Recall', 'Specificity', 'F-measure']]

    t_precision = []; t_recall = []; t_spec = []; t_fmeasure = []
    for file in images:
        # ground truth image
        img_ground_truth_path = GT_DATABASE_PATH + file

        # predicted image
        pred_img = datasets.replace_file_extension(DATASET, 'gt', 'pre', file)
        img_predicted_path = PREDICTED_DATABASE_PATH + pred_img

        TN, TP, FN, FP = test_image(img_predicted_path, img_ground_truth_path)
        PR, RE, SP, FM = print_metrics(str(file), TN, TP, FN, FP, data, is_print_sample)
        t_precision.append(PR)
        t_recall.append(RE)
        t_spec.append(SP)
        t_fmeasure.append(FM)

    # compute the mean of the measures, less fmeasure that must be calculated
    m_precision = np.mean(t_precision)
    m_recall = np.mean(t_recall)
    m_specificity = np.mean(t_spec)
    m_fmeasure = metrics.fmeasure(m_precision, m_recall)
    # compute the var of the measures, less fmeasure var that must be calculated
    var_precision = np.var(t_precision)
    var_recall = np.var(t_recall)
    var_spec = np.var(t_spec)
    var_fmeasure = np.mean(np.abs(np.asarray(t_fmeasure) - m_fmeasure)**2)


    if is_print_total:
        print '{0:60} {1:20} {2:20} {3:20} {4:20}'.format('Total', \
            '{0:.4f}'.format(m_precision), '{0:.4f}'.format(m_recall), \
            '{0:.4f}'.format(m_specificity), '{0:.4f}'.format(m_fmeasure))
    data.append(['Total', '{0:.4f}'.format(m_precision), \
        '{0:.4f}'.format(m_recall), '{0:.4f}'.format(m_specificity), \
        '{0:.4f}'.format(m_fmeasure)])

    write_metrics(data)

    return ([m_precision, m_recall, m_specificity, m_fmeasure], \
            [var_precision, var_recall, var_spec, var_fmeasure])


def print_metrics(label, TN, TP, FN, FP, data, is_print_sample):
    PR = metrics.precision(TP, FP)
    RE = metrics.recall(TP, FN)
    SP = metrics.specificity(TN, FP)
    Fmeasure = metrics.fmeasure(PR, RE)

    data.append([label, '{0:.4f}'.format(PR), '{0:.4f}'.format(RE), \
        '{0:.4f}'.format(SP), '{0:.4f}'.format(Fmeasure)])

    if is_print_sample:
        print '{0:60} {1:20} {2:20} {3:20} {4:20}'.format(label, \
        '{0:.4f}'.format(PR), '{0:.4f}'.format(RE), '{0:.4f}'.format(SP), \
        '{0:.4f}'.format(Fmeasure))

    return PR, RE, SP, Fmeasure


def write_metrics(data):
    with open(PREDICTED_DATABASE_PATH + DATASET + '_metrics.csv', 'wb') as fp:
        a = csv.writer(fp, delimiter=',')
        a.writerows(data)


def abspath(path):
    if path:
        return os.path.abspath(path)


def segment_images(plot_trapezia=True, is_log=True):
    images = os.listdir(ORI_DATABASE_PATH)
    dataset_files = []

    for file in images:
        predicted_img_name = imaging.get_img_name_from_path(file, '') + \
                            datasets.get_files_extension(DATASET, 'pre')
        predict_path = PREDICTED_DATABASE_PATH + predicted_img_name

        filename = ORI_DATABASE_PATH + file
        if is_log:
            log(filename)

        if platform.system() == 'Windows':
            command = abspath(SKIN_DETECTOR_BIN) +' '+ filename +' '+ \
                        predict_path +' '+ str(Y_MIN_PERCENTILE) +' '+\
                        STR(Y_MAX_PERCENTILE)

        else:
            command = [abspath(SKIN_DETECTOR_BIN), filename, predict_path, \
                        str(Y_MIN_PERCENTILE), str(Y_MAX_PERCENTILE)]

        proc = Popen(command, stdin=PIPE, stdout=PIPE, stderr=PIPE)
        # wait for process exit
        stdout, stderr = proc.communicate()

        if plot_trapezia:
            extract_log_and_draw_trapezia(filename, stdout, \
                PREDICTED_TRAPEZIA_PATH)


def extract_log_and_draw_trapezia(filename, log_content, output_dir):
    match = re.search(r"(Y0: [0-9]+).*(Y1: [0-9]+).*(Y2: [0-9]+).*(Y3: [0-9]+)" +\
        r".*(Ymin: [0-9]+).*(Ymax: [0-9]+).*(CRmin: [0-9]+).*(CRmax: [0-9]+)." +\
        r"*(CBmin: [0-9]+).*(CBmax: [0-9]+)", log_content)
    if (match):
        y0 = int(match.group(1).replace('Y0: ', ''))
        y1 = int(match.group(2).replace('Y1: ', ''))
        y2 = int(match.group(3).replace('Y2: ', ''))
        y3 = int(match.group(4).replace('Y3: ', ''))
        Ymin = int(match.group(5).replace('Ymin: ', ''))
        Ymax = int(match.group(6).replace('Ymax: ', ''))
        CRmin = int(match.group(7).replace('CRmin: ', ''))
        CRmax = int(match.group(8).replace('CRmax: ', ''))
        CBmin = int(match.group(9).replace('CBmin: ', ''))
        CBmax = int(match.group(10).replace('CBmax: ', ''))

        img_ground_truth_path = GT_DATABASE_PATH + filename#.replace('.jpg', '.png')
        datasets.process_ycrcb_image(filename)
        datasets.plot_ycrcb_image(output_dir, filename.replace('.jpg', '.data'),\
         Ymin, y0, y1, y2, y3, Ymax, CRmin, CRmax, CBmin, CBmax, save_plot=False)


def segment_db_images(is_log=True):
    try:
        os.remove(PREDICTED_DATABASE_PATH)
    except OSError:
        pass
    datasets.mk_predicted_dir(PREDICTED_DATABASE_PATH)
    #datasets.mk_predicted_dir(PREDICTED_TRAPEZIA_PATH)
    segment_images(plot_trapezia=False, is_log=is_log)


def reset_global_variables():
    global SKIN_DETECTOR_BIN, GT_DATABASE_PATH, PREDICTED_DATABASE_PATH, \
            ORI_DATABASE_PATH, PREDICTED_TRAPEZIA_PATH, GT_SKIN, GS_DATA_PATH, \
            GS_PDF_PATH, GS_CSV_PATH

    if platform.system() == 'Windows':
        SKIN_DETECTOR_BIN = '../bin/windows/skin-detector-'+ \
                            SKIN_DETECTOR_VAR +'.exe'
    else:
        SKIN_DETECTOR_BIN = '../bin/linux/skin-detector-'+ \
                            SKIN_DETECTOR_VAR

    GT_DATABASE_PATH = '../datasets/img_databases/'+ DATASET +'/GT/'
    PREDICTED_DATABASE_PATH = '../datasets/img_databases/'+ DATASET +\
                            '/PREDICTED/'+ SKIN_DETECTOR_VAR + '/'
    ORI_DATABASE_PATH = '../datasets/img_databases/'+ DATASET +'/ORI/'

    PREDICTED_TRAPEZIA_PATH = PREDICTED_DATABASE_PATH + 'trapezia/'
    GT_SKIN = 0 if DATASET == 'hgr' else 765

    GS_DATA_PATH = '../grid_search/'+ DATASET +'.npy'
    GS_PDF_PATH = '../grid_search/'+ DATASET +'.pdf'
    GS_CSV_PATH = '../grid_search/'+ DATASET +'.csv'


def save_metrics(results, is_remove=False):
    results_path = '../grid_search/'+ DATASET + '.npy'
    if is_remove:
        try:
            os.remove(results_path)
        except OSError:
            log('Previous '+ results_path +' file not found.')

    np.save(results_path, results)


def save_sort_metrics(data):
    """
    Save the data by the column indexes, where #5 fmeasure, #2 precision, and
    #3 recall.
    """
    data = data[np.lexsort((-data[:,3], -data[:,2], -data[:,5]))]

    # do not save the errors
    formats = '%d,%d' + ',%.4f'*4
    np.savetxt(GS_CSV_PATH, data[:,:6], fmt=formats)


def generate_plot(series_idx, ax, series, p_min):
    recalls = series['recall']
    precisions = series['precision']
    fmeasures = series['fmeasure']

    ax.set_xlim([0, 100])
    ax.set_ylim([0.25, 1])
    ax.yaxis.set_ticks(np.arange(0.25, 1, 0.15))
    ax.xaxis.set_ticks(np.arange(0, 100, 10))
    ax.tick_params(axis='both', which='both', length=0, labelsize=7)
    #ax.set_xlabel(r'$P_{max}$', fontsize=8)
    if (series_idx % 2 == 0):
        ax.set_ylabel(r'Measures', fontsize=7)

    ax.set_title(r'$P_{{min}}={{{}}}$'.format(p_min), fontsize=8)

    ax.plot(precisions[series_idx][0], precisions[series_idx][1], \
        color='g', marker='o', linewidth=0.5, markersize=2, label='Precision')
    ymin = precisions[series_idx][1] - precisions[series_idx][2]
    ymax = precisions[series_idx][1] + precisions[series_idx][2]
    ax.fill_between(precisions[series_idx][0], ymax, ymin, color='g', alpha=0.3)

    ax.plot(recalls[series_idx][0], recalls[series_idx][1], color='b', \
        marker='o', linewidth=0.5, markersize=2, label='Recall')
    ymin = recalls[series_idx][1] - recalls[series_idx][2]
    ymax = recalls[series_idx][1] + recalls[series_idx][2]
    ax.fill_between(recalls[series_idx][0], ymax, ymin, color='b', alpha=0.3)

    ax.plot(fmeasures[series_idx][0], fmeasures[series_idx][1], \
        color='r', marker='o', linewidth=0.5, markersize=2, label='F-measure')
    ymin = fmeasures[series_idx][1] - fmeasures[series_idx][2]
    ymax = fmeasures[series_idx][1] + fmeasures[series_idx][2]
    ax.fill_between(fmeasures[series_idx][0], ymax, ymin, color='r', alpha=0.3)


options = sys.argv[1:]
option = ''

if len(options):
    option = options[0]

if option == '1':
    reset_global_variables()
    segment_db_images()

elif option == '2':
    compute_metrics()

elif option == '3':
    # binarize the images from a given dataset (used once for sfa)
    images = os.listdir(GT_DATABASE_PATH)
    for file in images:
        # ground truth image
        img_ground_truth_path = GT_DATABASE_PATH + file
        print img_ground_truth_path
        # original image
        img_output_path = GT_DATABASE_PATH + file.replace('.jpg', '.png')
        imaging.binarize_image(img_ground_truth_path, img_output_path)

elif option == '4':
    datasets.generate_cr_histogram_from_image(ORI_DATABASE_PATH + '\obama.jpg')

elif option == '5':
    datasets.mk_predicted_dir(PREDICTED_DATABASE_PATH)
    datasets.mk_predicted_dir(PREDICTED_TRAPEZIA_PATH)
    file = 'obama.jpg'
    filename = ORI_DATABASE_PATH + file
    out_filename = PREDICTED_DATABASE_PATH + file.replace('.jpg', '.bmp')

    command = abspath(SKIN_DETECTOR_BIN) +' '+ filename +' '+ out_filename
    proc = Popen(command, stdin=PIPE, stdout=PIPE, stderr=PIPE)
    # wait for process exit
    stdout, stderr = proc.communicate()

    extract_log_and_draw_trapezia(filename, stdout, PREDICTED_TRAPEZIA_PATH)

elif option == '6':
    # we need to perform for a unique method, so combined is enough
    SKIN_DETECTOR_VAR = 'cmb'

    for d in ['hgr']:
        log('Starting grid search experiment for dataset: '+ d)
        log(80*'=')

        series = []
        for p_min in range(5, 100, 5):
            for p_max in range(p_min, 100, 5):
                print 'P_min: '+ str(p_min) + '  P_max: '+ str(p_max),
                sys.stdout.flush()
                DATASET = d
                Y_MIN_PERCENTILE = p_min
                Y_MAX_PERCENTILE = p_max

                reset_global_variables()
                segment_db_images(is_log=False)
                # append result metrics and variance
                measures, variance = compute_metrics(is_print_sample=False)
                results = np.append(measures, variance)

                series.append(np.append([p_min, p_max], results))

        save_metrics(series)


elif option == '7':
    for dataset in ['pratheepan', 'sfa', 'compaq', 'hgr']:
        DATASET = dataset
        reset_global_variables()

        data = np.load(GS_DATA_PATH)
        save_sort_metrics(data)

        # build the series object to plot
        series = {'precision':[], 'recall':[], 'fmeasure':[]}
        for p_min in range(5, 100, 5):
            rows = data[np.where(data[:,0] == p_min)]

            series['precision'].append([rows[:,1], rows[:,2], rows[:,6]])
            series['recall'].append([rows[:,1], rows[:,3], rows[:,7]])
            series['fmeasure'].append([rows[:,1], rows[:,5], rows[:,9]])

        log('Plot grid search results for '+ DATASET)
        pdf = PdfPages(GS_PDF_PATH)
        plt.rcParams['font.size'] = 8
        plt.rcParams['legend.fontsize'] = 8
        with plt.style.context('bmh'):
            fig = plt.figure(figsize=(6,8))
            fig.subplots_adjust(wspace=0.15, hspace=0.4, top=0.95)

            p_min = 5
            plot_idx = 0
            i = 0
            # using len of precision, but all must be of same lenght
            while i != len(series['precision']):
                ax = fig.add_subplot(5, 2, plot_idx+1)
                generate_plot(i, ax, series, p_min)
                p_min += 5
                plot_idx += 1

                # page break, keep only 10 charts per page
                if (plot_idx % 10 == 0):
                    ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.2),
                          fancybox=True, ncol=4, fontsize=7)
                    pdf.savefig(fig, bbox_inches='tight', pad_inches=0)

                    plot_idx = 0 # reset the index to start a new fig/page
                    fig = plt.figure(figsize=(6,8))
                    fig.subplots_adjust(wspace=0.15, hspace=0.4, top=0.95)

                i += 1

            ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.2),
                  fancybox=True, ncol=4, fontsize=7)

            pdf.savefig(fig, bbox_inches='tight', pad_inches=0)
            pdf.close()

elif option == '8':
    # experiment of specificity in non skin compaq images
    DATASET = 'compaq_ns'
    for method in ['cmb', 'ngh', 'cbs', 'crs']:
        SKIN_DETECTOR_VAR = method
        log('Non skin images testing with Compaq using '+ SKIN_DETECTOR_VAR)

        reset_global_variables()
        segment_db_images(is_log=False)
        # append result metrics and variance
        measures, variance = compute_metrics(is_print_sample=False)

else:
    log('Invalid option ['+ option +']')