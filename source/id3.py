from id3.data import Data
from id3.tree import learn

from common.logger import log
import common.imaging as imaging
import common.dataset as datasets

from PIL import Image

import numpy as np
import os, shutil
import random

# You can change the dataset to be used here
SFA_DATASET_PATH = "../datasets/sfa/skin_non_skin.data"
UCI_DATASET_PATH = "../datasets/uci/Skin_NonSkin.txt"

# SFA ground truth images database path
SFA_GT_DATABASE_PATH = "../img_databases/sfa/GT/"

# SFA original images database path
SFA_ORI_DATABASE_PATH = "../img_databases/sfa/ORI/"

# Both datasets were built using BGR order rather than RGB.
# The prediction/decision is also done over BGR order.
# But the images are in the RGB pixel order, so pay attention on it.

DATASET_PATH = SFA_DATASET_PATH

SKIN_IMG_DATABASE = "../img_databases/sfa/SKIN/15/"
NON_SKIN_IMG_DATABASE = "../img_databases/sfa/NS/15/"

TEST_OUTPUT_DIR = './id3/test_output/'

SKIN = '1'
NON_SKIN = '-1'


def train_id3_model():
    log('Loading the dataset from '+ DATASET_PATH)

    d = Data()
    d.load_from_file(DATASET_PATH, 3)
    d._set_debug(['B', 'G', 'R'])

    log('Training the tree to fit the model...')
    tree = learn(d)
    log('Training finished')


def test_image(img_path):
    rgb_img = imaging.open_rgb_image(img_path)
    arr = np.asarray(rgb_img)
    result = np.copy(arr)

    width, height = rgb_img.size
    
    log("Image size "+ str(width) + "x" +str(height))

    skin = 0
    non_skin = 0
    for x in range(0, width):
        for y in range(0, height):
            r, g, b = rgb_img.getpixel((x, y))
            decision = tree.decide([str(b), str(g), str(r)])

            if decision == SKIN:
                result[y,x] = [r, g, b]
                skin += 1
            if decision == NON_SKIN:
                result[y,x] = [0, 0, 0]
                non_skin += 1
    
    log('Pixels classification :: skin - '+ str(skin) +' non skin - '+ str(non_skin))
    new_im = Image.fromarray(result, 'RGB')
    new_im.show()


def test_random_images_from_sfa(images_list, images_number=10):
    '''
    Get random images from the SFA_ORI_DATABASE_PATH to test based on the trainned
    model.
    '''
    log("Removing previous test output folder '" + TEST_OUTPUT_DIR + "'")
    shutil.rmtree(TEST_OUTPUT_DIR, ignore_errors=True)

    if not os.path.exists(TEST_OUTPUT_DIR):
        os.makedirs(TEST_OUTPUT_DIR)

    files = []
    for (dirpath, dirnames, filenames) in os.walk(SFA_ORI_DATABASE_PATH):
        files.extend(filenames)
        break

    random_images = random.sample(files, images_number)
    if images_list:
        # when available, we use the images list given
        random_images = images_list

    for img in random_images:
        # ground truth image
        img_ground_truth_path = SFA_GT_DATABASE_PATH + img

        # original image
        img_original_path = SFA_ORI_DATABASE_PATH + img

        # test the original image and concat the GT
        source_img, predicted_img = test_image(img_original_path, img_ground_truth_path)
        arr_merged = imaging.concat_images(np.asarray(source_img), np.asarray(predicted_img))

        # get the image name without the file extension
        im_merged_name = os.path.basename(img)
        im_merged_name = os.path.splitext(im_merged_name)[0]


        # concat the merged image with the ground truth
        arr_ground_truth = np.asarray(imaging.open_rgb_image(img_ground_truth_path))
        arr_merged = imaging.concat_images(arr_merged, arr_ground_truth)

        merged_img = Image.fromarray(arr_merged, 'RGB')
        merged_img.save(TEST_OUTPUT_DIR + im_merged_name + '_merged.jpg')


def test_image(img_path, img_ground_truth_path):
    rgb_img = imaging.open_rgb_image(img_path)
    rgb_gt_img = imaging.open_rgb_image(img_ground_truth_path)
    result = np.copy(np.asarray(rgb_img))

    width, height = rgb_img.size
    log_message = "Image size "+ str(width) + "x" + str(height)
    log_message += " :: Number of samples "+ str(width*height)

    confusion_matrix = [[0, 0],
                        [0, 0]]
    '''
    confunsion matrix
          -------------------------------------
          |       Yes       |       No        |
    -------------------------------------------
    | Yes | True Positive   | False Negative  |
    -------------------------------------------
    | No  | False Positive  | True Negative   |
    -------------------------------------------
    '''
    for x in range(0, width):
        for y in range(0, height):
            r, g, b = rgb_img.getpixel((x, y))
            r1, g1, b1 = rgb_gt_img.getpixel((x, y))
            # TODO: check the dataset order of channels
            # this reshape is due scikit-learn warning
            decision = tree.decide([str(b), str(g), str(r)])

            if decision == SKIN:
                result[y, x] = [r, g, b]
                if (r1+g1+b1 != 0): # skin pixel from ground truth
                    confusion_matrix[0][0] += 1 # true positive
                else:
                    confusion_matrix[0][1] += 1 # false negative
            if decision == NON_SKIN:
                result[y, x] = [0, 128, 0]
                if (r1+g1+b1 != 0): # skin pixel from ground truth
                    confusion_matrix[1][0] += 1 # false positive
                else:
                    confusion_matrix[1][1] += 1 # true negative

    # gets the image name without the file extension
    im_original_name = os.path.basename(img_path)
    im_original_name = os.path.splitext(im_original_name)[0]

    log(80*'=')
    log(im_original_name)
    log(80*'=')
    log(log_message)

    false_positive = confusion_matrix[1][0]
    false_negative = confusion_matrix[0][1]
    true_positive  = confusion_matrix[0][0]
    true_negative  = confusion_matrix[1][1]
    total = true_negative + true_positive + false_negative + false_positive

    error_rate = None
    if total > 0:
        error_rate = (false_positive + false_negative) / total

    log('Confusion Matrix')
    log(29 * '-')
    log('|\t'+ str(true_positive) +'\t | \t'+ str(false_negative) +'\t|')
    log(29 * '-')
    log('|\t'+ str(false_positive) +'\t | \t'+ str(true_negative) +'\t|')
    log(29 * '-')
    log('Error Rate')
    log(str(error_rate) +'\n')

    # creates a new image based on predicted result array
    new_im = Image.fromarray(result, 'RGB')

    return rgb_img, new_im


def get_menu():
    return input("\n-------------------------------------"
                 "\n| Tell me what you wanna do:        |" +
                 "\n| 1 - train the tree                |" +
                 "\n| 2 - test a sample                 |" +
                 "\n| 3 - test a whole image            |" +
                 "\n| 4 - test random images            |" +
                 "\n| 5 - exit                          |" +
                 "\n-------------------------------------\n")

option = get_menu()

while option != '5':
    if option == '1':
        train_id3_model()
    
    elif option == '2':
        entry = input("Pass the BGR code using space (e.g. 124 151 28)\n")
        rgb = entry.split(" ")

        if len(rgb) != 3:
            print ("BGR must have 3 int values\n")
        else:
            label = tree.decide([rgb[2], rgb[1], rgb[0]])
            label_type = "skin" if label == SKIN else "non skin"
            print (entry + ' is ' + label_type)
    
    elif option == '3':
        img_path = input("Give me the image path from here (e.g. ./imgs/data/face.jpg)\n")
        test_image(img_path)

    elif option == '4':
        images = ['img (727).jpg', 'img (840).jpg', 'img (991).jpg', 'img (907).jpg',
                  'img (133).jpg']
        test_random_images_from_sfa(images)

    option = get_menu()