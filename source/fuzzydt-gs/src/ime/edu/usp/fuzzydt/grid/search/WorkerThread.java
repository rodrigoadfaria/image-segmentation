package ime.edu.usp.fuzzydt.grid.search;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

public class WorkerThread implements Runnable {

	private List<String> result;
	private String task, dataset;

	WorkerThread(String task) {
		this.task = task;
		// the dataset name is the first part of the task name
		this.dataset = this.task.split(" ")[0];
	}

	public void run() {
		result = new ArrayList<String>();
		try {
			System.out.println("Startig "+ task);
			String taskPath = task.replace(" ", "-");
			File dir = new File(taskPath);
			if (!dir.exists()) {
				dir.mkdir();
				new File(dir.getPath() + File.separator + "lib").mkdir();
			}
			
			File fuzzydt = new File("FuzzyDT.jar");
			copyFile(dir, fuzzydt, false);

			File libs = new File("lib"+ File.separator +"Clas_Multiple.jar");
			copyFile(dir, libs, true);
			libs = new File("lib"+ File.separator +"weka.jar");
			copyFile(dir, libs, true);
			
			File data = new File(this.dataset + ".dat");
			copyFile(dir, data, false);

			String command = "cd "+ dir.getPath()+ " && java -jar "+ fuzzydt.getPath() +" "+ task;
			String[] shellCommandResult = {};

			if (Utils.isWindows()) {
				String[] os = Utils.concat(Utils.WIN_RUNTIME, Utils.WIN_RUNTIME_OPTIONS);
				shellCommandResult = Utils.concat(os, new String[] { command });
			} else {
				String[] os = Utils.concat(Utils.UNIX_RUNTIME, Utils.UNIX_RUNTIME_OPTIONS);
				shellCommandResult = Utils.concat(os, new String[] { command });
			}

			Process process = Runtime.getRuntime().exec(shellCommandResult, null, null);
			StreamWorker stdin = new StreamWorker(process.getInputStream(), StreamWorker.INPUT);
			stdin.start();
			StreamWorker error = new StreamWorker(process.getErrorStream(), StreamWorker.ERROR);
			error.start();

			process.waitFor();
			stdin.join();
			error.join();
		} catch (Exception e1) {
			System.out.println("Error on "+ task);
			e1.printStackTrace();
		}

		System.out.println("Finished "+ task);
		for (String line : result) {
			System.out.println(line);
		}
		System.out.println("---------------------------------------------");
	}

	private void copyFile(File dir, File fuzzydt, boolean isLib) throws IOException {
		String target = dir.getPath() + File.separator + fuzzydt.getName();
		if (isLib)
			target = dir.getPath() + File.separator + "lib" + File.separator + fuzzydt.getName();

		Files.copy(fuzzydt.toPath(),
					(new File(target).toPath()),
					StandardCopyOption.REPLACE_EXISTING);
	}
	
	/**
	 * 
	 * This thread is used in order to not block the container thread
	 * during the stream reader.
	 */
	class StreamWorker extends Thread {
		public static final String INPUT = "i";
		public static final String ERROR = "e";

		private InputStream is;
		private String type;

		StreamWorker(InputStream is, String type) {
			this.is = is;
			this.type = type;
		}

		public void run() {
			String charset = (Utils.isWindows()) ? "Cp850" : StandardCharsets.UTF_8.name();
	        
			try {
				InputStreamReader isr = new InputStreamReader(is, charset);
				BufferedReader br = new BufferedReader(isr);
				String line = null;
				while ((line = br.readLine()) != null) {
					if (INPUT.equals(this.type)) {
						result.add(line);
					} else if (ERROR.equals(this.type)) {
						System.out.println(line);
					}
				}
			} catch (IOException ioe) {
				System.out.println("Error reading stream for command");
			}
		}
	}
}