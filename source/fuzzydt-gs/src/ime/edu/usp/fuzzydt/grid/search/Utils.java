package ime.edu.usp.fuzzydt.grid.search;

import java.util.Arrays;
import java.util.Locale;

public class Utils {

	public enum OSType {
		Windows, MacOS, Linux, Other
	};

	public static final String[] WIN_RUNTIME = { "cmd.exe" };
	public static final String[] WIN_RUNTIME_OPTIONS = { "/C" };
	public static final String[] UNIX_RUNTIME = { "/bin/bash" };
	public static final String[] UNIX_RUNTIME_OPTIONS = { "-l", "-c" };

	public static OSType getOperatingSystemType() {
		String OS = System.getProperty("os.name", "generic").toLowerCase(Locale.ENGLISH);
		if ((OS.indexOf("mac") >= 0) || (OS.indexOf("darwin") >= 0)) {
			return OSType.MacOS;
		} else if (OS.indexOf("win") >= 0) {
			return OSType.Windows;
		} else if (OS.indexOf("nux") >= 0) {
			return OSType.Linux;
		} else {
			return OSType.Other;
		}
	}

	public static boolean isWindows() {
		return getOperatingSystemType() == OSType.Windows;
	}

	public static <T> T[] concat(T[] first, T[] second) {
		T[] result = Arrays.copyOf(first, first.length + second.length);
		System.arraycopy(second, 0, result, first.length, second.length);

		return result;
	}
}
