package ime.edu.usp.fuzzydt.grid.search;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import sfuzzysemig.CombinatoricException;

public class FuzzyDTGridSearch {

	public static final String SEPARATOR = " ";

	public static void main (String args[]) throws IOException, CombinatoricException, Exception {
		int jobs = 1;
		if (args != null && args.length > 0) {
			String jobsParam = args[0];
			jobs = Integer.parseInt(jobsParam);
		}

		String[] datasets = new String[]{"skin_non_skin_lab", "skin_non_skin_rgb", "skin_non_skin_hsv", "skin_non_skin_ycrcb"};
		String[] partitionMethods = new String[]{"wm", "infogain", /*"rf",*/ "fixed"};
		String[] confidenceLevels = new String[]{"10", "15", "20", "25"};

		// used in the case of fixed partition method choosen
		String[] partitions = new String[]{"2", "3", "4", "5", "6", "7", "8", "9"};

		List<String> grid = new ArrayList<String>();
		for (String dataset : datasets) {
			for (String partitionMethod : partitionMethods) {
				for (String level : confidenceLevels) {
					String fit = dataset+ SEPARATOR +partitionMethod+ SEPARATOR +level;

					if (partitionMethod.equals("fixed")) {
						for (String partition : partitions) {
							grid.add(fit+ SEPARATOR +partition);
						}
					} else {
						grid.add(fit);
					}
				}
			}
		}

		System.out.println("Total of "+grid.size() +" fits for the FuzzyDT");

		ExecutorService tpes =
				Executors.newFixedThreadPool(jobs);

		WorkerThread[] workers = new WorkerThread[grid.size()];
		for (int i = 0; i < grid.size(); i++) {
			workers[i] = new WorkerThread(grid.get(i));
			tpes.execute(workers[i]);
		}
		tpes.shutdown();
	}
}
