from __future__ import division
from common.logger import log
import common.imaging as imaging

from PIL import Image
import os
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import cv2
import time

from sklearn.model_selection import train_test_split

# these datasets were produced from skin/non skin image patches from sfa
SFA_LAB_DATASET_PATH   = '../datasets/sfa/skin_non_skin_lab.data'
SFA_YCRCB_DATASET_PATH = '../datasets/sfa/skin_non_skin_ycrcb.data'
SFA_HSV_DATASET_PATH   = '../datasets/sfa/skin_non_skin_hsv.data'
SFA_HSI_DATASET_PATH   = '../datasets/sfa/skin_non_skin_hsi.data'
SFA_HSL_DATASET_PATH   = '../datasets/sfa/skin_non_skin_hsl.data'
SFA_RGB_DATASET_PATH   = '../datasets/sfa/skin_non_skin_rgb.data'
SFA_BGR_DATASET_PATH   = '../datasets/sfa/skin_non_skin_bgr.data'

UCI_DATASET_PATH = '../datasets/uci/Skin_NonSkin.txt'

# color spaces
COLOR_LAB   = 'Lab'
COLOR_YCRCB = 'YCrCb'
COLOR_HSV   = 'HSV'
COLOR_HSI   = 'HSI'
COLOR_HSL   = 'HSL'
COLOR_RGB   = 'RGB'
COLOR_BGR   = 'BGR'

# a dictionary containg the images extension in each dataset
# ori -> original images
# gt  -> ground truth images
# pre -> predicted images by a learner
extensions = {
    'pratheepan': {
        'ori': '.jpg',
        'gt' : '.png',
        'pre': '.bmp',
    },
    'hgr': {
        'ori': '.jpg',
        'gt' : '.bmp',
        'pre': '.bmp',
    },
    'sfa': {
        'ori': '.jpg',
        'gt' : '.png',
        'pre': '.bmp',
    },
    'compaq': {
        'ori': '.jpg', # compaq has jpg and gif images, but we converted to jpg
        'gt' : '.pbm',
        'pre': '.bmp',
    },
    'compaq_ns': {
        'ori': '.jpg', # this dataset is part of the compaq non skin only
        'gt' : '.jpg',
        'pre': '.bmp',
    }
}

# symbol used between the columns within the dataset file
SPLIT = '\t'


def get_files_extension(dataset, subset):
    """
    Returns the image extension within a subset of images of the given dataset.
    e.g. 'hgr' and subset 'gt' for ground truth images
    """
    return extensions[dataset][subset]


def replace_file_extension(dataset, subset_from, subset_to, img_name):
    """
    Replace the image extension from a subset of images of the given dataset, 
    to another.
    e.g. 'hgr' and subset 'gt' for ground truth images, to predicted 'pre'
    """
    return img_name.replace(get_files_extension(dataset, subset_from),
                            get_files_extension(dataset, subset_to))


def get_dataset_path_by_color_space(color_space):
    """
    Retrieve the dataset path based on the color space given.
    """
    if color_space == COLOR_LAB:
        return SFA_LAB_DATASET_PATH
    elif color_space == COLOR_YCRCB:
        return SFA_YCRCB_DATASET_PATH
    elif color_space == COLOR_HSV:
        return SFA_HSV_DATASET_PATH
    elif color_space == COLOR_HSI:
        return SFA_HSI_DATASET_PATH
    elif color_space == COLOR_HSL:
        return SFA_HSL_DATASET_PATH
    elif color_space == COLOR_BGR:
        return SFA_BGR_DATASET_PATH
    else: # default rgb
        return SFA_BGR_DATASET_PATH


def get_channels_by_color_space(color_space):
    """
    Retrieve the channel's names based on the color space given.
    """
    if color_space == COLOR_YCRCB:
        return ['Y', 'Cr', 'Cb']
    else: # default 3 channels
        return list(color_space)


def generate_sfa_dataset(skin_img_database, skin_label, nskin_img_database, \
    nskin_label, color_space=COLOR_BGR):
    """
    Remove the latest dataset based in the path dataset_path and
    recreate it with skin/non skin image samples.
    Use this method only for the SFA image database.
    """
    dataset_path = get_dataset_path_by_color_space(color_space)
    log('Removing previous dataset ' + dataset_path)
    try:
        os.remove(dataset_path)
    except OSError:
        log("File '"+ dataset_path +"' not found. Let's generate it...")
    
    log('Processing the image database using color space '+ color_space)
    process_images(dataset_path, skin_img_database, skin_label, color_space)
    process_images(dataset_path, nskin_img_database, nskin_label, color_space)


def process_images(dataset_path, img_database_path, pattern, color_space):
    """
    Process an image database based in the given img_database_path
    labelling the pixels of each image with a pattern (skin/non skin).
    Use this method only for the sfa image database.
    """
    log('Generating samples in '+ dataset_path +' with class '+ str(pattern))
    images = os.listdir(img_database_path)
    for file in images:
        im = cv2.imread(img_database_path + file)
        append_pattern(dataset_path, im, pattern, color_space)

    # it is better to not remove duplicate entries at all
    #remove_duplicate_entries(dataset_path)


def append_pattern(dataset_path, im, pattern, color_space):
    '''
    Open the image in color_space format and save the pixel color_space
    channels according to the given pattern in the dataset_path.
    Use this method only for the sfa image database.
    '''
    image_array = np.asarray(im)
    if color_space == COLOR_LAB:
        img = cv2.cvtColor(image_array, cv2.COLOR_BGR2LAB)
    elif color_space == COLOR_YCRCB:
        img = cv2.cvtColor(image_array, cv2.COLOR_BGR2YCR_CB)
    elif color_space == COLOR_HSV:
        img = cv2.cvtColor(image_array, cv2.COLOR_BGR2HSV)
    elif color_space == COLOR_HSI:
        img = cv2.cvtColor(image_array, cv2.COLOR_BGR2HSI)
    elif color_space == COLOR_HSL:
        img = cv2.cvtColor(image_array, cv2.COLOR_BGR2HSL)
    elif color_space == COLOR_RGB:
        img = cv2.cvtColor(image_array, cv2.COLOR_BGR2RGB)
    elif color_space == COLOR_BGR:
        img = cv2.cvtColor(image_array, cv2.COLOR_RGB2BGR)
    else:
        raise NameError('A valid color space must be provided')

    ch1, ch2, ch3 = cv2.split(img)
    ch1 = ch1.flatten()
    ch2 = ch2.flatten()
    ch3 = ch3.flatten()
    # the channels must be of the same length
    assert(len(ch1) == len(ch2) == len(ch3))

    f = open(dataset_path, 'a+')

    # we can use either channel 1, 2 or 3 - all of them must be the same length
    for i in range(len(ch1)):
        f.write(str(ch1[i]) + SPLIT + str(ch2[i]) + SPLIT + str(ch3[i]) \
            + SPLIT + str(pattern) + '\n')


def remove_duplicate_entries(dataset_path):
    """
    Create a copy of the dataset given by dataset_path in a tmp file,
    remove the older temporary file  - if exists - and create a new one
    removing the duplicate entries.
    """
    log('Removing duplicate entries')
    dataset_path_tmp = dataset_path + '.tmp'
    try:
        os.remove(dataset_path_tmp)
    except OSError:
        log("Previous '"+ dataset_path_tmp +"' file not found. \
            Removing duplicate entries from "+ dataset_path)
    
    os.rename(dataset_path, dataset_path_tmp)
    
    lines_seen = set() # holds lines already seen
    outfile = open(dataset_path, "w")
    for line in open(dataset_path_tmp, "r"):
        if line not in lines_seen: # not a duplicate
            lines_seen.add(line)
            outfile.write(line)
            
    outfile.close()


def rename_uci_non_skin_labels(dataset_path):
    """
    Create a copy of the dataset given by dataset_path in a tmp file,
    remove the older temporary file - if exists - and create a new one
    changing the labels whose value are '2' - non skin by '-1', due the
    function error used after in some experiments.
    """
    log('Changing the class names of non skin samples in '+ dataset_path)
    dataset_path_tmp = dataset_path + '.tmp'
    try:
        os.remove(dataset_path_tmp)
    except OSError:
        log('Previous '+ dataset_path_tmp +' file not found.')

    outfile = open(dataset_path_tmp, "w")
    for line in open(dataset_path, "r"):
        # gets the content of the fourth column and ignores the line breaker
        columns = line.split('\t')
        label = str(columns[3]).replace('\r\n', '')

        if label == '2':
            # we get the last char to rename it
            line = line[:-3] + line[-3:].replace('2', '-1')

        outfile.write(line)

    outfile.close()
    os.rename(dataset_path_tmp, dataset_path)


def plot_dataset(dataset='sfa', color_space=COLOR_BGR, plot_nskin=True):
    """
    Plot the dataset according to the given dataset param and color space.
    Use 'uci' or 'sfa'. The last one is the default.
    The default color space is RGB.
    """
    dataset_path = get_dataset_path_by_color_space(color_space)
    log('Plotting dataset '+ dataset_path)

    if dataset == 'uci':
        file = open(UCI_DATASET_PATH)
    else:
        file = open(dataset_path)
    file.readline()

    data = np.loadtxt(file)

    # test the class present and split out the label column
    nonskin_idx = np.where(data[:,3] == -1)
    nonskin = data[nonskin_idx][:, 0:3]
    # fix seed to keep track of the indexes
    np.random.seed(0)
    nonskin = nonskin[np.random.choice(nonskin.shape[0], 10000)]

    skin_idx = np.where(data[:,3] == 1)
    skin = data[skin_idx][:, 0:3]

    generate_cbcr_histogram_from_data(skin[:,2], is_cr=False, save_plot=True)
    generate_cbcr_histogram_from_data(skin[:,1], save_plot=True)

    # fix seed to keep track of the indexes
    np.random.seed(1)
    skin = skin[np.random.choice(skin.shape[0], 10000)]

    matplotlib.rcParams.update({'font.size': 14})
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.tick_params(length=0, labelsize=14)
    ax.set_autoscalex_on(False)
    ax.set_autoscaley_on(False)
    ax.set_autoscalez_on(False)
    ax.set_xlim([data[:,0].min(), data[:,0].max()])
    ax.set_ylim([data[:,1].min(), data[:,1].max()])
    ax.set_zlim([data[:,2].min(), data[:,2].max()])

    # plot points in 3D
    output_file = dataset +'_'+ color_space +'_skin_only.eps'
    ax.scatter(skin[:,0], skin[:,1], skin[:,2], color='#3253DC', marker='.')
    if plot_nskin:
        output_file = dataset +'_'+ color_space +'.eps'
        ax.scatter(nonskin[:,0], nonskin[:,1], nonskin[:,2], color='#6AB19B', marker='.')
    output_file = '../'+ output_file

    # the order used in the dataset are RGB, for this color space
    x, y, z = get_channels_by_color_space(color_space)
    ax.set_xlabel(x, fontweight='bold')
    ax.set_ylabel(y, fontweight='bold')
    ax.set_zlabel(z, fontweight='bold')

    fig.savefig(output_file, bbox_inches='tight', \
        pad_inches=0, format='eps', dpi=600)
    #plt.close(fig)


def plot_sample_2d_dataset(dataset='sfa', color_space=COLOR_HSV):
    """
    Plot the dataset according to the given dataset param and color space
    for just two of the three channels. We are also sampling some rows from
    the database to be enhance the visibility.
    Use 'uci' or 'sfa'. The last one is the default.
    The default color space is RGB.
    """
    dataset_path = get_dataset_path_by_color_space(color_space)
    log('Plotting dataset '+ dataset_path)

    file = open(dataset_path)
    file.readline()

    data = np.loadtxt(file)

    # test the class present and split out the label column
    nonskin_idx = np.where(data[:,3] == -1)
    nonskin = data[nonskin_idx][:, 0:3]
    nonskin = nonskin[np.random.choice(nonskin.shape[0], 160)]

    skin_idx = np.where(data[:,3] == 1)
    skin = data[skin_idx][:, 0:3]
    skin = skin[np.random.choice(skin.shape[0], 80)]

    fig = plt.figure()
    ax = fig.gca()
    ax.set_autoscalex_on(False)
    ax.set_autoscaley_on(False)
    ax.set_xlim([0, 200])
    ax.set_ylim([0, 200])

    # plot points in 2D using two channels
    ax.plot(skin[:,1], skin[:,2], '.', markersize=8, color='#1B55C0')
    ax.plot(nonskin[:,1], nonskin[:,2], '.', markersize=8, color='#488C13')

    # you must change the order and name of the channels here
    ax.set_xlabel('a')
    ax.set_ylabel('b')

    plt.show()


def load_sfa(color_space=COLOR_BGR):
    """
    Loads the SFA dataset based on the given color space.
    If none is passed, we use RGB as default.
    """
    dataset_path = get_dataset_path_by_color_space(color_space)

    log('Reading the dataset '+ dataset_path)
    f = open(dataset_path)

    data = np.loadtxt(f)
    samples = data[:, 0:3]
    target = data[:, 3]

    return samples, target


def load_uci():
    log('Reading the dataset '+ UCI_DATASET_PATH)
    f = open(UCI_DATASET_PATH)

    data = np.loadtxt(f)
    samples = data[:, 0:3]
    target = data[:, 3]

    return samples, target


def load_data(dataset='sfa', split=True, color_space=COLOR_BGR):
    """
    Loads the database based on dataset parameter using the split strategy.
    """
    data_samples = []
    data_target = []
    X_test = []
    Y_test = []

    if dataset == 'uci':
        data_samples, data_target = load_uci()
    else:
        data_samples, data_target = load_sfa(color_space)

    # If the split param is True, we split the dataset with 30% for test.
    # On the other hand, we use a dataset to train and another to test
    if split == True:
        # here we give the test size and the random state parameter which is a
        # pseudo-random number generator state used for random sampling.
        X_train, X_test, Y_train, Y_test = train_test_split(data_samples, \
            data_target, test_size=0.3, random_state=77)

        return X_train, Y_train, X_test, Y_test
    else:
        return data_samples, data_target


def mk_predicted_dir(path):
    """
    Creates the predicted directory needed to keep outcome of segmented images
    """
    directory = os.path.dirname(path)
    if not os.path.exists(directory):
        os.makedirs(directory)


def make_histograms():
    make_histogram(h_color='blue', color_space=COLOR_BGR)
    make_histogram(h_color='red', color_space=COLOR_YCRCB)
    make_histogram(h_color='orange', color_space=COLOR_HSV)
    make_histogram(h_color='green', color_space=COLOR_LAB)


def make_histogram(h_color='blue', color_space=COLOR_BGR):
    samples, target = load_sfa(color_space)
    non_skin_idx = np.where(target == -1)[0]
    skin_idx = np.where(target == 1)[0]

    if not os.path.isdir('./histograms/'+ color_space):
        os.mkdir('./histograms/'+ color_space)

    channels = get_channels_by_color_space(color_space)
    for i in range(3):
        non_skin_channel = samples[non_skin_idx][:, i]
        skin_channel = samples[skin_idx][:, i]
        channel = channels[i]

        generate_histogram(h_color, channel, color_space, non_skin_channel, \
            skin_channel)


def generate_histogram(h_color, title, color_space, nskin_channel, skin_channel):
    fig = plt.figure()
    ax = fig.gca()
    ax.set_autoscalex_on(False)
    ax.set_autoscaley_on(False)
    ax.set_xlim([0, 300])
    ax.set_ylim([0, 200000])

    plt.hist(nskin_channel, bins=25, color=h_color, alpha=0.3)
    plt.title(title)
    plt.xlabel("Value")
    plt.ylabel("Frequency")
    fig.savefig('./histograms/'+ color_space +'/'+ title +'_nskin.png', dpi=200)
    plt.close()

    fig = plt.figure()
    ax = fig.gca()
    ax.set_autoscalex_on(False)
    ax.set_autoscaley_on(False)
    ax.set_xlim([0, 300])
    ax.set_ylim([0, 200000])

    plt.hist(nskin_channel, bins=25, color=h_color, alpha=0.3)
    plt.hist(skin_channel, bins=25, color=h_color)
    plt.title(title)
    plt.xlabel("Value")
    plt.ylabel("Frequency")
    fig.savefig('./histograms/'+ color_space +'/'+ title + \
        '_skin_nskin.png', dpi=200)
    plt.clf()
    plt.close(fig)


def plot_ycrcb_dataset(dataset='sfa', color_space=COLOR_YCRCB):
    """
    Plot the dataset according to the given dataset param and color space
    for just two of the three channels. We are also sampling some rows from
    the database to enhance the visibility. Use 'uci' or 'sfa'.
    The last parameter is the color_space; we are using YCrCb as default.
    """
    dataset_path = get_dataset_path_by_color_space(color_space)
    log('Plotting dataset '+ dataset_path)

    file = open(dataset_path)
    file.readline()

    data = np.loadtxt(file)

    # we are interested only in the skin samples
    skin_idx = np.where(data[:,3] == 1)
    skin = data[skin_idx][:, 0:3]
    skin = skin[np.random.choice(skin.shape[0], 32000)]

    fig = plt.figure()
    ax = fig.gca()
    ax.set_autoscalex_on(False)
    ax.set_autoscaley_on(False)
    ax.set_xlim([0, 255])
    ax.set_ylim([0, 255])

    # plot points in 2D using two channels
    ax.plot(skin[:,0], skin[:,1], '.', markersize=8, color='#F3802E')
    ax.plot(skin[:,0], skin[:,2], '.', markersize=8, color='#17B1E1')

    # you must change the order and name of the channels here
    ax.set_xlabel('Y')
    ax.set_ylabel('Cr/Cb')

    plt.show()


def plot_ycrcb_image(output_path, dataset_path, Ymin, y0, y1, y2, y3, Ymax, \
    CRmin, CRmax, CBmin, CBmax, save_plot=False):
    """
    Plot the dataset according to the given dataset param and color space
    for just two of the three channels.
    We also plot the trapezia into the chart according the trapezia params.
    """
    log('Plotting dataset '+ dataset_path)
    """
    file = open(dataset_path)
    file.readline()

    data = np.loadtxt(file)

    # we are interested only in the skin samples
    skin_idx = np.where(data[:,3] == 1)
    skin = data[skin_idx][:, 0:3]
    #skin = skin[np.random.choice(skin.shape[0], 32000)]
    nskin_idx = np.where(data[:,3] == -1)
    nskin = data[nskin_idx][:, 0:3]
    #nskin = nskin[np.random.choice(nskin.shape[0], 32000)]
    """
    matplotlib.rcParams.update({'font.size': 20})
    fig = plt.figure(facecolor='white')
    ax = fig.gca()
    #ax.set_autoscalex_on(False)
    #ax.set_autoscaley_on(False)
    ax.set_xlim([0, 270])
    #ax.set_ylim([0, 255])

    #ax.grid(color='#C7C8C9', linestyle='-', linewidth=1)
    ax.spines['right'].set_color('none')
    ax.spines['top'].set_color('none')
    ax.spines['bottom'].set_color('#C7C8C9')
    ax.spines['left'].set_color('#C7C8C9')
    ax.xaxis.set_ticks_position('bottom')
    ax.yaxis.set_ticks_position('left')

    # plot points in 2D using two channels
    #ax.plot(skin[:,0], skin[:,1], '.', markersize=8, color='#F3802E')
    #ax.plot(skin[:,0], skin[:,2], '.', markersize=8, color='#17B1E1')

    trapezia_x = [Ymin, y0, y1, Ymax, Ymin, Ymin, y2, y3, Ymax, Ymin]
    trapezia_y = [CRmin, CRmax, CRmax, CRmin, CRmin, CBmax, CBmin, CBmin, \
                  CBmax, CBmax]

    ax.plot(trapezia_x[:5], trapezia_y[:5], marker='o', linestyle='-', \
        markersize=6, color='#F26C56')
    ax.plot(trapezia_x[5:], trapezia_y[5:], marker='o', linestyle='-', \
        markersize=6, color='#0096A9')
    ax.plot((y0, y0), (Ymin, CRmax), 'k:', color='#C7C8C9')
    ax.plot((Ymin, y0), (CRmax, CRmax), 'k:', color='#C7C8C9')
    ax.plot((y1, y1), (Ymin, CRmax), 'k:', color='#C7C8C9')

    ax.plot((y2, y2), (Ymin, CBmin), 'k:', color='#C7C8C9')
    ax.plot((Ymin, y2), (CBmin, CBmin), 'k:', color='#C7C8C9')
    ax.plot((y3, y3), (Ymin, CBmin), 'k:', color='#C7C8C9')
    ax.plot((Ymax, Ymax), (Ymin, CBmax), 'k:', color='#C7C8C9')

    # Py point
    # TODO: THIS MUST BE COMPUTED DYNAMICALLY
    ax.plot((y0-30, y0-30), (Ymin, CRmax-9), 'k:', color='#C7C8C9')
    ax.plot((Ymin, y0-30), (CRmax-9, CRmax-9), 'k:', color='#C7C8C9')
    ax.plot([y0-30], [CRmax-9], marker='o', markersize=6, color='#636466')
    ax.plot((Ymin, y0-30), (CBmax-24, CBmax-24), 'k:', color='#C7C8C9')
    ax.plot([y0-30], [CBmax-24], marker='o', markersize=6, color='#636466')

    # set x/y tick labels
    ax.set_xticks([Ymin, y0-30, y0, y2, y1, y3, Ymax, ax.get_xlim()[1]])
    ax.set_xticklabels([r'$Y_{min}$', r'$P_Y$', r'$Y_0$', r'$Y_2$', r'$Y_1$', \
        r'$Y_3$', r'$Y_{max}$', 'Y'])
    ax.set_yticks([CBmin, CBmax-24, CBmax, CRmin+3, CRmax-9, CRmax, CRmax + 20])
    ax.set_yticklabels([r'$Cb_{min}$', r'$H_{Cb}(P_Y)$', r'$Cb_{max}$', \
        r'$Cr_{min}$', r'$H_{Cr}(P_Y)$', r'$Cr_{max}$', 'Cb/Cr'])

    # h_cr and h_cb arrow plots
    plt.annotate(
        '', xy=(150, CRmin), xycoords='data',
        xytext=(150, CRmax), textcoords='data',
        arrowprops={'arrowstyle': '<->', 'color': '#C7C8C9'})
    plt.text(152, CRmin + ((CRmax - CRmin) / 2), r'$h_{cr}$')

    plt.annotate(
        '', xy=(150, CBmin), xycoords='data',
        xytext=(150, CBmax), textcoords='data',
        arrowprops={'arrowstyle': '<->', 'color': '#C7C8C9'})
    plt.text(152, CBmin + ((CBmax - CBmin) / 2), r'$h_{cb}$')

    if save_plot:
        title =  os.path.basename(dataset_path)
        title =  os.path.splitext(title)[0]
        fig.savefig(output_path + title + '.eps', format='eps', dpi=600)
        plt.clf()
        plt.close(fig)
    else:
        plt.show()


def process_ycrcb_image(img_database_path):
    """
    Process a image database based in the given img_database_path
    labelling the pixels of each image with a pattern (skin/non skin).
    Use this method only for the sfa image database.
    """
    # get the image name without the file extension
    dataset_dir = os.path.dirname(img_database_path)
    dataset_path = os.path.basename(img_database_path)
    dataset_path = dataset_dir +'\\'+ os.path.splitext(dataset_path)[0] + '.data'

    log('Generating samples in '+ dataset_path)
    im = cv2.imread(img_database_path)

    image_array = np.asarray(im)
    img = cv2.cvtColor(image_array, cv2.COLOR_BGR2YCR_CB)

    Y, cr, cb = cv2.split(img)
    Y = Y.flatten()
    cr = cr.flatten()
    cb = cb.flatten()
    # the channels must be of the same length
    assert(len(Y) == len(cr) == len(cb))

    f = open(dataset_path, 'w')

    # we can use either channel 1, 2 or 3
    for i in range(len(Y)):
        if (Y[i] + cr[i] + cb[i] != 0): # skin pixel from ground truth
            f.write(str(Y[i]) + SPLIT + str(cr[i]) + SPLIT + str(cb[i]) \
                + SPLIT + str(1) + '\n')
        else:
            f.write(str(Y[i]) + SPLIT + str(cr[i]) + SPLIT + str(cb[i]) \
                + SPLIT + str(-1) + '\n')

    return dataset_path


def generate_cr_histogram_from_image(img_path, cr_max=0, save_plot=False):
    """
    Plot the histogram of Cr values with the Cr range [133, 183] as well as
    the point where Crmax is located - 0 in the x axis if none is given.
    """
    matplotlib.rcParams.update({'font.size': 16})

    im = cv2.imread(img_path)
    img = cv2.cvtColor(im, cv2.COLOR_BGR2YCR_CB)
    height, width = img.shape[:2]

    # 0.1% of Cr histogram pixels to calculate Crmax
    percent = int((height * width) * 0.1/100)

    Y, cr, cb = cv2.split(img)
    Y = Y.flatten()
    cr = cr.flatten()
    cb = cb.flatten()
    # the channels must be of the same length
    assert(len(Y) == len(cr) == len(cb))

    fig = plt.figure(facecolor='white')
    ax = fig.gca()
    ax.set_xlim([0, 255])
    ax.grid(color='#C7C8C9', linestyle='-', linewidth=1)
    ax.spines['right'].set_color('#C7C8C9')
    ax.spines['bottom'].set_color('#C7C8C9')
    ax.xaxis.set_ticks_position('bottom')
    ax.yaxis.set_ticks_position('left')

    # the histogram of the data
    plt.hist(cr, bins=256, range=(0, 255), color='#F26C56',
        histtype='stepfilled', edgecolor='none')

    plt.xlabel('Cr')
    plt.ylabel('Number of Pixels')
    ylim = ax.get_ylim()[1]

    # draw a horizontal line to indicate where is 0.1% of pixels
    plt.plot((0, 255), (percent, percent), 'k:', linewidth=2)
    # draw vertical lines for Cr range [133, 183]
    plt.plot((133, 133), (0, ylim), 'k--', linewidth=1, color='#0096A9')
    plt.plot((183, 183), (0, ylim), 'k--', linewidth=1, color='#0096A9')
    # draw the point where is Crmax
    plt.plot(cr_max, percent, 'ro', color='#636466')
    # draw informational texts
    plt.text(5, 3*percent, '0.1% of pixels')
    plt.text(134, ylim * 0.9, 'Cr = [133, 183]')

    if save_plot:
        dataset_dir = os.path.dirname(img_path)
        title = os.path.basename(img_path)
        title = dataset_dir +'\\'+ os.path.splitext(title)[0]
        fig.savefig(title + '_cr_histogram.eps', bbox_inches='tight', \
            format='eps', dpi=600)
        plt.clf()
        plt.close(fig)
    else:
        plt.show()


def generate_cbcr_histogram_from_data(data, is_cr=True, save_plot=False):
    """
    Plot the histogram of Cr values with the Cr range [133, 183] as well as
    the point where Crmax is located - 0 in the x axis if none is given.
    """
    matplotlib.rcParams.update({'font.size': 22})

    fig = plt.figure(facecolor='white')
    ax = fig.gca()
    ax.set_xlim([0, 255])
    ax.set_ylim([0, 20000])
    ax.grid(color='#C7C8C9', linestyle='-', linewidth=1)
    ax.spines['right'].set_color('#C7C8C9')
    ax.spines['bottom'].set_color('#C7C8C9')
    ax.xaxis.set_ticks_position('bottom')
    ax.yaxis.set_ticks_position('left')

    # the histogram of the data
    hist_color = '#F26C56' if is_cr else '#0096A9'
    plt.hist(data, bins=256, range=(0, 255), color=hist_color,
        histtype='stepfilled', edgecolor='none')

    plt.ylabel('Number of Pixels')
    ylim = ax.get_ylim()[1]

    output_file = '../sfa_cr_histogram.eps'
    if is_cr:
        plt.xlabel('Cr')

        # draw vertical lines for Cr range [133, 183]
        plt.plot((133, 133), (0, ylim), 'k--', linewidth=1, color='#0096A9')
        plt.plot((183, 183), (0, ylim), 'k--', linewidth=1, color='#0096A9')
        # draw informational texts
        plt.text(134, ylim * 0.9, 'Cr = [133, 183]')

    else:
        output_file = '../sfa_cb_histogram.eps'
        plt.xlabel('Cb')

        # draw vertical lines for Cb range [77, 127]
        plt.plot((77, 77), (0, ylim), 'k--', linewidth=1, color='#F26C56')
        plt.plot((127, 127), (0, ylim), 'k--', linewidth=1, color='#F26C56')
        # draw informational texts
        plt.text(78, ylim * 0.9, 'Cb = [77, 127]')

    if save_plot:
        fig.savefig(output_file, bbox_inches='tight', \
            format='eps', dpi=600)
    else:
        plt.show()


def count_samples_repeatability(dataset='uci'):
    X_train, Y_train, X_test, Y_test = load_data(dataset)

    positive_train_samples = X_train[np.where(Y_train == 1)]
    negative_train_samples = X_train[np.where(Y_train == -1)]
    positive_test_samples = X_test[np.where(Y_test == 1)]
    negative_test_samples = X_test[np.where(Y_test == -1)]

    log('dataset: '+ dataset +' '+ str(X_train.shape[0] + X_test.shape[0]) + ' samples')
    log(80*'-')
    log('+ train '+ str(positive_train_samples.shape[0]))
    log('- train '+ str(negative_train_samples.shape[0]))
    log('+ test '+ str(positive_test_samples.shape[0]))
    log('- test '+ str(negative_test_samples.shape[0]))

    counting_pos = 0
    for A in positive_test_samples:
        for B in positive_train_samples:
            if np.array_equal(A, B):
                counting_pos += 1
                break

    log('+ test samples seen in training '+ str(counting_pos) + \
        ' ('+ str((counting_pos / positive_test_samples.shape[0] * 100)) + '%)')


    counting_neg = 0
    for A in negative_test_samples:
        for B in negative_train_samples:
            if np.array_equal(A, B):
                counting_neg += 1
                break

    log('- test samples seen in training '+ str(counting_neg) + \
        ' ('+ str((counting_neg / negative_test_samples.shape[0] * 100)) + '%)')


def count_num_pixels_in_dataset(dataset_path):
    log('Counting pixels in '+ dataset_path)
    images = os.listdir(dataset_path)
    pixels = 0
    for file in images:
        pixels += imaging.get_num_pixels(dataset_path + file)

    return pixels