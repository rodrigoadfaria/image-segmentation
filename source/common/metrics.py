# this package is important to division to work
from __future__ import division


'''
confunsion matrix
      -------------------------------------
      |       Yes       |       No        |
-------------------------------------------
| Yes | True Positive   | False Negative  |
-------------------------------------------
| No  | False Positive  | True Negative   |
-------------------------------------------
'''

def precision(tp, fp):
    """
    Also called positive predictive value (PPV) is the fraction of relevant
    instances among the retrieved instances.
    """
    if tp == 0 and fp == 0:
        return 0.0

    return tp / (tp + fp)


def recall(tp, fn):
    """
    Also called the true positive rate (TPR), the sensitivity,
    or probability of detection.
    """
    if tp == 0 and fn == 0:
        return 0.0

    return tp / (tp + fn)


def specificity(tn, fp):
    """
    Also called the true negative rate (TNR) measures the proportion of
    negatives that are correctly identified as such (e.g. the percentage of
    healthy people who are correctly identified as not having the condition).
    """
    if tn == 0 and fp == 0:
        return 0.0

    return tn / (tn + fp)


def fmeasure(precision, recall):
    """
    Also called F-score or F1-score, is a measure of a test's accuracy.
    F-measure is the harmonic average of the precision and recall, where an
    score reaches its best value at 1 (perfect precision and recall)
    and worst at 0.
    """
    if precision == 0 or recall == 0:
        return 0.0

    return 2 * ((precision * recall) / (precision + recall))


def fpr(tn, fp):
    """
    A false positive ratio (or false alarm ratio) is the probability of falsely
    rejecting the null hypothesis for a particular test.
    """
    if tn == 0 and fp == 0:
        return 0.0

    return fp / (tn + fp)