import pygame

class Webcam(object):

    def __init__(self):
        self.size = (640, 480)
        pygame.camera.init()
        # create a display surface. standard pygame stuff
        self.display = pygame.display.set_mode(self.size, 0)
        
        # this is the same as what we saw before
        self.clist = pygame.camera.list_cameras()
        if not self.clist:
            raise ValueError("Sorry, no cameras detected.")
        self.cam = pygame.camera.Camera(self.clist[0], self.size)
        self.cam.start()

        # create a surface to capture to. For performance purposes
        # bit depth is the same as that of the display surface.
        self.snapshot = pygame.surface.Surface(self.size, 0, self.display)

    def get_and_flip(self):
        # if you don't want to tie the framerate to the camera, you can check 
        # if the camera has an image ready. Note that while this works
        # on most cameras, some will never return true.
        if self.cam.query_image():
            self.snapshot = self.cam.get_image(self.snapshot)

        # blit it to the display surface - simple!
        self.display.blit(self.snapshot, (0, 0))
        pygame.display.flip()

    def snapshot(self):
        status = True
        while status:
            events = pygame.event.get()
            for e in events:
                if (e.type == pygame.KEYDOWN and e.key == pygame.K_f):
                    print ("tecla f prescionada..")
                    if self.cam.query_image():
                        image = self.cam.get_image()
                        pygame.image.save(image, "fotoTesteML.bmp")
                        self.cam.stop()
                        status = False
                        pygame.display.quit()
                        return
                if e.type == pygame.QUIT or (e.type == pygame.KEYDOWN and e.key == pygame.K_ESCAPE):
                    # close the camera safely
                    self.cam.stop()
                    status = False
                    pygame.display.quit()
                    return

            self.get_and_flip()