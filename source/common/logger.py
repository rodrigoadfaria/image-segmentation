from time import gmtime, strftime
import sys

LOG_PATH = "running.log"

def log(msg):
    """
    Prints out the given message with the current time
    and write it to an log file
    """
    line = str(strftime("%a, %d %b %Y %H:%M:%S ", gmtime())) + msg
    print (line)
    sys.stdout.flush()

    #outfile = open(LOG_PATH, "a")
    #outfile.write(line + '\n')
    #outfile.close()
