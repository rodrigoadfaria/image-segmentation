import numpy as np
from PIL import Image
import os
import fnmatch
from time import sleep
from common.logger import log


LABELME_DATABASE_PATH='..\datasets\img_databases\label-me\\'
CIFAR_DATABASE_PATH='..\datasets\img_databases\cifar-100\\'

# setup these variables accordingly the dataset
IMG_DATABASE_PATH=CIFAR_DATABASE_PATH
IMG_DATABASE_FILES_FILTER='*.png'

def concat_images(imga, imgb):
    """
    Combines two color image ndarrays side-by-side.
    """
    ha,wa = imga.shape[:2]
    hb,wb = imgb.shape[:2]
    max_height = np.max([ha, hb])
    total_width = wa + wb
    new_img = np.zeros(shape=(max_height, total_width, 3), dtype=np.uint8)
    new_img[:ha, :wa] = imga
    new_img[:hb, wa:wa+wb] = imgb

    return new_img


def open_rgb_image(path):
    """
    Open the image given in the path as RGB.
    """
    try:
        im = Image.open(path)
        return im.convert('RGB')
    except Exception:
        log("File '"+ path +"' not found.")


def gen_mosaic(image_path, lines=25, columns=25, tile_height=32, tile_width=32,
    gray_scale=False, dynamic_shape=False):
    """
    First, we create the area for the mosaic and select the images to compound
    it based on the computed height x width.
    After that, we calculate the center of the mosaic to put the image into and
    we return the original image instance, mosaic path as well as the height and
    width where the original image is positioned.
    """
    files = []
    for (dirpath, dirnames, filenames) in os.walk(IMG_DATABASE_PATH):
        for filename in fnmatch.filter(filenames, IMG_DATABASE_FILES_FILTER):
            files.append(os.path.join(dirpath, filename))

    img = Image.open(image_path)
    img_array = np.array(img)
    img_h = img_array.shape[1]
    img_w = img_array.shape[0]

    # if this variable is true, we compute the mosaic width and height dynamicaly
    # increasing by a factor of these measures
    if dynamic_shape:
        lines = int(img_h * 1.5) / tile_height
        columns = int(img_w * 1.5) / tile_width

    mosaic_h=lines*tile_height
    mosaic_w=columns*tile_width
    mosaic = Image.new("RGB", (mosaic_h, mosaic_w))

    idx=0
    for h in range(lines):
        for w in range(columns):
            if gray_scale:
                im = Image.open(files[idx]).convert('L')
            else:
                im = Image.open(files[idx])
            mosaic.paste(im, (h*tile_height, w*tile_width))
            idx = idx + 1

    # compute where the image will fit into the mosaic
    pos_height = (mosaic_h - img_h) / 2
    pos_width = (mosaic_w - img_w) / 2

    mosaic.paste(img, (pos_height, pos_width))

    # we create the mosaic file name based on input image
    mosaic_fname = get_img_name_from_path(image_path, '_mosaic')
    mosaic.save(mosaic_fname)

    return img, mosaic_fname, pos_height, pos_width


def crop_image(img_path, p_height, p_width, p_height_end, p_width_end):
    """
    Crops the image based on the height x width parameters given.
    """
    img = Image.open(img_path)
    w,h = img.size

    img = img.crop((p_height, p_width, p_height_end, p_width_end))
    img.save(img_path)


def get_img_name_from_path(image_path, suffix):
    """
    Return the image name based on the given path.
    """
    try:
        image_name_start = image_path.rindex('\\') + 1
    except Exception:
        image_name_start = 0
    image_name_end = image_path.rindex('.')

    image_name = image_path[image_name_start:image_name_end]

    if suffix:
        return image_path.replace(image_name, image_name + suffix)

    return image_name


def binarize_image(img_path, img_output_path):
    """
    Binarize an image by setting up white into the pixels we have color
    (no black, which is the background)
    """
    rgb_img = open_rgb_image(img_path)
    img_array = np.array(rgb_img)

    width, height = rgb_img.size

    for x in range(0, width):
        for y in range(0, height):
            r, g, b = img_array[y, x]
            decision = r + g + b

            if decision != 0: # background black pixel
                img_array[y, x] = [255,255,255]

    im_output = Image.fromarray(img_array)
    im_output.save(img_output_path)


def get_num_pixels(filepath):
    """
    Get the number of pixels in an image
    """
    width, height = Image.open(open(filepath)).size
    return width*height