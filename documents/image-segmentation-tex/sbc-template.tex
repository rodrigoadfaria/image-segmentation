\documentclass[12pt]{article}

\usepackage{sbc-template}
\usepackage{graphicx,url}
\usepackage[brazil]{babel}
\usepackage[utf8]{inputenc}
\usepackage{amsfonts}
\usepackage{amsmath,mathrsfs}
\usepackage{color, colortbl}
\usepackage{graphicx}

\graphicspath{ {imgs/} }

\definecolor{Gray}{gray}{0.9}
\definecolor{LightCyan}{rgb}{0.88,1,1}
% UTF-8 encoding is recommended by ShareLaTex

     
\sloppy

\title{Segmentação de Regiões de Pele e Não Pele em Imagens}

\author{Rodrigo Augusto Dias Faria\inst{1}, Rodrigo Romano Teixo\inst{1} }


\address{Instituto de Matemática e Estatística -- Universidade de São Paulo (IME-USP)\\
  Departamento de Ciência da Computação
  \email{\{rofaria, rodteixo\}@ime.usp.br, \{rodrigoadfaria, rodteixo\}@gmail.com}
}

\begin{document} 

\maketitle

\section{Motivação}

Frequentemente, o ser humano é confrontado com problemas de tomada de decisão que, em geral, são resolvidos através de experiências vividas no passado.

Entretanto, para que se possa atingir de forma satisfatória uma decisão correta, o indivíduo baseia-se em um aprendizado prévio que em problemas complexos, tornam a solução inviável, uma vez que os experimentos podem consumir tanto recurso financeiro, quanto de tempo.

Com a evolução de técnicas computacionais como a aprendizagem computacional, este processo pode ser automatizado de forma que um sistema possa gerar uma solução ou fornecer sugestões de soluções possíveis.
Aliada ao processamento de imagens, a aprendizagem computacional torna passível de resolução, por exemplo, o problema de detecção de faces em imagens no espaço de cores R, G, B, \cite{uciml:12} que é o propósito deste trabalho.

Sendo assim, a proposta é criar um sistema computacional capaz de classificar pixels que caracterizem pele de não pele e realizar a segmentação de uma determinada região de uma imagem no espaço de cores R, G, B, conforme ilustra as Figuras \ref{fig:minipage1} e \ref{fig:minipage2} \cite{sfa13} que representam, respectivamente, uma imagem de uma face frontal e o seu \textit{ground truth} obtida com ferramentas como o Photoshop.

\begin{figure}[ht]
    \centering
    \begin{minipage}[b]{0.45\linewidth}
    \includegraphics[width=.5\textwidth]{img1.jpg}
    \caption{Imagem original da face.}
    \label{fig:minipage1}
\end{minipage}
\quad
\begin{minipage}[b]{0.45\linewidth}
    \includegraphics[width=.5\textwidth]{img2.jpg}
    \caption{Ground truth da imagem de entrada.}
    \label{fig:minipage2}
    \end{minipage}
\end{figure}


\section{Problema a ser resolvido}

O objetivo é, a partir de um modelo de segmentação de pele/não-pele, construir uma ferramenta computacional capaz de extrair informações de uma determinada imagem possibilitando a geração de áreas de contorno, como da face, por exemplo, permitindo que aplicações de detecção de face humana, detecção de raça, expressões, possam ser mais facilmente elaboradas.

As bases de dados a serem utilizadas contêm imagens de várias texturas de pele e não-pele obtidas a partir de milhares de imagens arbitrárias de faces de diferentes idades, sexo e raças e estão disponíveis no Color FERET Image Database \cite{nist} e PAL Face Database \cite{pal-texas}.

\section{\textit{Dataset}}

O \textit{dataset} utilizado no experimento contém 51.445 amostras, compostas por 3 atributos que constituem o vetor de entradas $x = [x_1, x_2, x_3]$, $x \in \mathbb{R}^{d}$, sendo $d$ a dimensão do espaço, e que representam, respectivamente, os canais B (\textit{Blue}), G (\textit{Green}) e R (\textit{Red}) do modelo de cores RGB, além de uma quarta coluna que determina a classe a qual a amostra $x$ pertence, denotada por $y$, sendo $y \in Y$ e $Y = \{+1, -1\}$.

Em outras palavras, cada amostra é um pixel RGB com um determinado rótulo. Como tem-se os dados rotulados com exemplos de qual é a saída correta para uma dada entrada, o problema estudado é categorizado como sendo de aprendizado supervisionado \cite{mostafa12}.

A Tabela \ref{tbl-dataset} exemplifica um pequeno trecho do \textit{dataset}. Vale ressaltar que, do total das $N$ amostras, 36.791 são de pixels não pele e 14.654 de pixels com diferentes tons de pele.

\begin{table}[h]
\centering
\caption{Algumas amostras do \textit{dataset}.}\vspace*{0.2cm}
\label{tbl-dataset}
\begin{tabular}{|l|l|l|l|} \hline
R      & G      & B      & Clase  \\ \hline
77     & 89     & 129    &  1     \\
156    & 185    & 230    &  1     \\
211    & 222    & 255    &  1     \\
81     & 87     & 132    &  1     \\
58     & 61     & 29     & -1     \\
60     & 37     & 15     & -1     \\
0      & 141    & 254    & -1     \\
151    & 156    & 111    & -1     \\ 
\ldots & \ldots & \ldots & \ldots \\ \hline
\end{tabular}
\end{table}

Uma vez que a dimensão do espaço do problema é $d = 3$, tornou-se possível que os dados fossem plotados para melhor interpretação dos mesmos, conforme mostram as Figuras \ref{fig:minipage3} e \ref{fig:minipage4}.
\begin{figure}[ht]
    \centering
    \begin{minipage}[b]{0.45\linewidth}
    \includegraphics[width=.8\textwidth]{data_plot.png}
    \caption{Total de 51.445 amostras plotadas em $d=3$.}
    \label{fig:minipage3}
\end{minipage}
\quad
\begin{minipage}[b]{0.45\linewidth}
    \includegraphics[width=.8\textwidth]{data_plot_skin.png}
    \caption{Gráfico com apenas pixels de tons de pele.}
    \label{fig:minipage4}
    \end{minipage}
\end{figure}


\section{Seleção do modelo}
Existem vários tipos de classificadores para reconhecimento de padrões, cada qual com suas vantagens e desvantagens. Tendo observado a estrutura dos dados, fica evidente que no lançamento de uma nova amostra não classificada, a distância entre este novo ponto e seus vizinhos pode ser determinante para a definição da classe a que ele pertence.

Sendo assim, o método escolhido para resolver o problema é o \textit{k-Nearest Neighbor} (knn), cujo algoritmo calcula a distância do novo ponto em relação aos demais $N$ pontos do \textit{dataset} e o classifica de acordo com os $k$ mais próximos \cite{mitchell:97}.

O algoritmo utilizado faz parte do pacote \textit{scikit-learn} \cite{scikit-learn}, que disponibiliza várias funções de distância, dentre as quais pode-se destacar:
\begin{align*}
f_{dist}(x, x_j) = \left\{ \begin{array}{rl}
                                &\mbox{ $\sqrt{\sum\limits_{j=1}^{N} {(x - x_j)}^2}$ }                       \quad $Euclidiana$ \\
                                &\mbox{ $\sum\limits_{j=1}^{N} {|x - x_j|}^2$ }                              \quad $Manhattan$ \\
                                &\mbox{ $\sum\limits_{j=1}^{N} {max|x - x_j|}$ }                             \quad $Chebyshev$\\
                                &\mbox{ ${\bigg(\sum\limits_{j=1}^{N} {|x - x_j|}^p\bigg)} ^ \frac{1}{p} $ } \quad $Minkowski$ \\
                                &\mbox{\ldots}
                           \end{array} \right.
\end{align*}
Onde $x$ é um ponto sendo classificado e $x_j$ é um ponto do \textit{dataset}.

Outro ponto importante considerado no modelo é a utilização de pesos na atenuação das distâncias para encontrar a função alvo $g(x)$. A primeira abordagem, denominada uniforme, não utiliza peso e é dada por:
\begin{align*}
    g(x) = \left\{ \begin{array}{rl} 
                      +1, &\mbox{ se $\sum\limits_{j=1}^{k} {y_j > 0}, \quad y_j \in NN$ } \\
                      -1, &\mbox{ caso contrário}
                   \end{array} \right.
\end{align*}
Onde $NN$ é o conjunto formado pelos $k$ pontos mais próximos de $x$, de acordo com alguma função de distância $f_{dist}(x, x_j), x_j \in D$, sendo $D = \{(x_1,y_1), (x_2,y_2), \ldots, (x_n,y_n) \}$ a distribuição dos pontos.

Na segunda abordagem, a distância é utilizada como peso $W$ na definição da classe, ou seja, os pontos mais próximos de $x$ têm uma maior influência na sua rotulação:
\begin{align*}
    W(x, x_j) = \frac{e^{-f_{dist}(x, x_j)}} {\sum\limits_{j=1}^{k} e^{-f_{dist}(x, x_j)}}
\end{align*}
E dessa forma:
\begin{align*}
    g(x) = \left\{ \begin{array}{rl} 
                      +1, &\mbox{ se $\sum\limits_{j=1}^{k} {y_j*W(x, x_j) > 0}, \quad x_j, y_j \in NN$ } \\
                      -1, &\mbox{ caso contrário}
                   \end{array} \right.
\end{align*}

\section{\textit{Cross-Validation}}
Aprender os parâmetros de uma função alvo e testá-la nos mesmos dados é um erro fundamental, pois o modelo repetiria os rótulos das amostras tão logo treinadas implicando em um ajuste perfeito dos dados, mas não seria suficiente para prever qualquer coisa útil sobre novos dados de entrada. Esta situação é chamada \textit{overfitting} e para evitá-la, o \textit{dataset} é então particionado de forma a armazenar parte dos dados para a etapa de teste, no intuito de encontrar o estimador com a melhor performance. Esta etapa é conhecida como \textit{cross-validation} \cite{mostafa12}.

Tendo definido o modelo, foram realizados experimentos com \textit{scikit-learn} tomando a estratégia $k-Fold = 10$, onde cada partição do \textit{dataset} fica com, aproximadamente, 5.144 amostras.

Com a otimização dos parâmetros, o estimador ótimo foi encontrado para o número de vizinhos $N\_neighbors = 100$ e utilizando os pesos $W$ para atenuação da distância, como pode ser visto na Tabela \ref{tbl-results}.
\begin{table}[h]
\centering
\caption{Tabela com os resultados do \textit{cross-validation}.}\vspace*{0.2cm}
\label{tbl-results}
\begin{tabular}{|l|l|l|l|l|}\hline
Fold & Média   & \begin{tabular}[c]{@{}l@{}}Desvio\\   padrão\end{tabular} & N\_neighbors & Weights  \\ \hline
1    & 0.97319 & 0.02095                                                   & 50           & distance \\
2    & 0.97273 & 0.02045                                                   & 50           & uniform  \\
\rowcolor{LightCyan}
3    & 0.97356 & 0.01938                                                   & 100          & distance \\
4    & 0.97267 & 0.02002                                                   & 100          & uniform  \\
5    & 0.97075 & 0.01976                                                   & 200          & distance \\
6    & 0.96857 & 0.02155                                                   & 200          & uniform  \\
7    & 0.96643 & 0.02430                                                   & 400          & distance \\
8    & 0.96276 & 0.02949                                                   & 400          & uniform  \\
9    & 0.95739 & 0.03514                                                   & 800          & distance \\
10   & 0.95115 & 0.04019                                                   & 800          & uniform  \\ \hline
\end{tabular}
\end{table}

O erro calculado foi de $E_{out} = 2.68\%$ e as \textit{learning curves} podem ser vistas na Figura \ref{fig:figure5}.
\begin{figure}[h]
\begin{minipage}[b]{0.8\linewidth}
    \centering
    \includegraphics[width=0.8\textwidth]{learning_curves_gx}
\end{minipage}
\caption{Learning curves após execução do experimento.}
\label{fig:figure5}
\end{figure}

\section{Resultados}
Os resultados da aplicação do $knn$ foram satisfatórios, considerando o $E_{out}$, como pode ser visto na Figura \ref{fig:figure6}. A área em verde foi apagada pelo algoritmo, pois os pixels foram classificados como de não pele.
 
\begin{figure}[hb]
\begin{minipage}[b]{0.23\linewidth}
    \centering
    \includegraphics[width=\textwidth]{brown}    
\end{minipage}
\hspace{0.05cm}
\begin{minipage}[b]{0.23\linewidth}
    \centering
    \includegraphics[width=\textwidth]{brown_knn}
\end{minipage}
\hspace{0.05cm}
\begin{minipage}[b]{0.23\linewidth}
    \centering
    \includegraphics[width=\textwidth]{blond}
\end{minipage}
\hspace{0.05cm}
\begin{minipage}[b]{0.23\linewidth}
    \centering
    \includegraphics[width=\textwidth]{blond_knn}
\end{minipage}
\caption{Imagens de face originais e após a classificação.}
\label{fig:figure6}
\end{figure}

Porém, dependendo das propriedades da imagem, tais como, ambiente em que foi obtida, condições de iluminação, manchas na pele, cabelo e outros objetos com tons de pele, nota-se que o modelo ainda produz algum erro, seja classificando um pixel de pele como não pele ou vice-versa.

Na tentativa de melhorar os resultados, um segundo $dataset$ foi elaborado com apenas uma imagem de uma face $ground$ $truth$, perfeitamente segmentada. Uma janela de tamanho $9 x 9$ é passada por toda a imagem gerando novos valores de entrada, sendo a classe do vetor $x$ de características determinada pelo pixel central da janela. O tamanho da janela $i _x j$ pode ser variável.

Antes da janela ser vetorizada em uma entrada do $dataset$, pequenas mutações (cerca de 5\%) são inseridas no retalho obtido com a janela, exceto no pixel central, para simular as correções que deseja-se que sejam aprendidas pela função alvo, denominada agora por $h(x)$, o que constitui uma composição de classificadores, já que o resultado da primeira classificação é utilizado no segundo.

A mesma estratégia de \textit{cross-validation} foi aplicada, sendo agora o $dataset$ de tamanho $N = 16660$, cada partição fica com aproximadamente 1.660 amostras, sendo que uma delas sempre é mantida para o teste e as outras nove são utilizadas no treinamento.

\begin{figure}[h]
\begin{minipage}[b]{0.8\linewidth}
    \centering
    \includegraphics[width=0.8\textwidth]{learning_curves_hx}
\end{minipage}
\caption{Learning curves após aplicação de $h(x)$.}
\label{fig:figure7}
\end{figure}

Com a otimização dos parâmetros, o estimador ótimo foi encontrado para o número de vizinhos $N\_neighbors = 3$ e utilizando os pesos $W$ para atenuação da distância.
O erro calculado foi de $E_{out} = 2.1\%$ e as \textit{learning curves} podem ser vistas na Figura \ref{fig:figure7}.

Como mostra o resultado, as imagens também têm uma melhoria na classificação após a aplicação de $h(x)$. A Figura \ref{fig:figure8} mostra a imagem da face com alguns realces na tentativa de corrigir pixels classificados erroneamente.
\begin{figure}[h]
\begin{minipage}[b]{0.23\linewidth}
    \centering
    \includegraphics[width=\textwidth]{blond}    
\end{minipage}
\hspace{0.05cm}
\begin{minipage}[b]{0.23\linewidth}
    \centering
    \includegraphics[width=\textwidth]{blond_knn}
\end{minipage}
\hspace{0.05cm}
\begin{minipage}[b]{0.23\linewidth}
    \centering
    \includegraphics[width=\textwidth]{blond_knn2}
\end{minipage}
\hspace{0.05cm}
\begin{minipage}[b]{0.23\linewidth}
    \centering
    \includegraphics[width=\textwidth]{blond_knn3}
\end{minipage}
\caption{Imagens da face original e após a classificação com dois realces para correção.}
\label{fig:figure8}
\end{figure}

\clearpage
\bibliographystyle{sbc}
\bibliography{sbc-template}

\end{document}
