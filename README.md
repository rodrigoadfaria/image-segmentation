# README #

This repository is intended to be room for image segmentation and skin color classification experiments.

### What is this repository for? ###

* Summary
Some machine learning experiments with skin color datasets.

* Version
0.1

### How do I get set up? ###

* Summary of set up
Clone this repo localy on your computer
Download the [image databases](https://bitbucket.org/rodrigoadfaria/image-segmentation/downloads/img_databases.zip) located at Downloads page. This will be used for some image extraction and segmentation experiments
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Who do I talk to? ###

If you have any doubt or suggestion, feel free to contact me by this wiki page or e-mail rodrigoadfaria[at]gmail[dot]com